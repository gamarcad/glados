from glados.encrypters import *
from glados.utils import *



def generate_gear(alpha : str) -> [str] : 
    GEAR = []
    for letter in alpha:
        GEAR.append(letter)
    return GEAR


GEAR6 = ['M', 'U', 'N', 'E', 'Y', 'X', 'B', 'H', 'Q', 'L', 'F', 'K', 'I', 'C', 'T', 'P', 'O', 'J', 'S', 'R', 'Z', 'A', 'G', 'W', 'V', 'D']


GEAR5 = ['K', 'I', 'V', 'G', 'U', 'S', 'O', 'W', 'F', 'E', 'R', 'L', 'X', 'H', 'Z', 'Q', 'P', 'D', 'T', 'B', 'M', 'N', 'A', 'Y', 'J', 'C']


GEAR4 = ['J', 'Q', 'H', 'K', 'I', 'W', 'S', 'V', 'L', 'O', 'F', 'Z', 'U', 'Y', 'P', 'X', 'T', 'B', 'G', 'E', 'N', 'R', 'M', 'D', 'C', 'A']

GEAR3 = ['H', 'G', 'X', 'B', 'C', 'Q', 'P', 'V', 'W', 'Y', 'Z', 'L', 'F', 'S', 'U', 'E', 'M', 'I', 'D', 'N', 'J', 'R', 'K', 'O', 'A', 'T']

GEAR2 = ['G', 'S', 'Q', 'X', 'Z', 'R', 'O', 'N', 'J', 'L', 'K', 'M', 'Y', 'D', 'T', 'A', 'I', 'W', 'C', 'E', 'H', 'B', 'U', 'P', 'V', 'F']

def findIndex(gear : [str] ,  init_letter : str) -> int :
    for i in range(len(gear)):
        if(gear[i] == init_letter):
            return i

def init_gear(gear : [str] , init_letter : str) -> [str] : 
    shifted_gear = []
    init_letter_index = findIndex(gear,init_letter)
    for i in range(len(gear)):
        shifted_gear.append(gear[(i+init_letter_index)%len(gear)])
    return shifted_gear

def init_gears(gears : [[str]] , init_word : str) -> [[str]] :
    shifted_gears = []
    for i in range(len(gears)) :
        shifted_gears.append(init_gear(gears[i],init_word[i]))
    return shifted_gears


def encrypt_enigma(text : str , clear_gear : [str] , cipher_gear1 : [str] , cipher_gear2 : [str] , init_word : [str]) -> str :
    print("BEGIN")
    switch = False
    text = clean(text)
    clear_gear,cipher_gear1,cipher_gear2 = init_gears([clear_gear,cipher_gear1,cipher_gear2],init_word[0])

    print(clear_gear)
    print(cipher_gear1)
    print(cipher_gear2)
    
    encrypt_text = [""]*len(text)
    for i in range(len(text)):
        shift_cg = findIndex(clear_gear,text[i])
        shift_cg1 = (-shift_cg)%ALPHABET_LENGTH +3

        if(switch):
            encrypt_text[i] = cipher_gear1[shift_cg1]
            switch = not switch
        else :
            encrypt_text[i] = cipher_gear2[shift_cg]
            switch = not switch

    return encrypt_text
    print("END")


def combi_init_word() -> str :
    res = []

    for i in range(ALPHABET_LENGTH):
        for j in range(ALPHABET_LENGTH):
            for k in range(ALPHABET_LENGTH):
                if( i != j and i != k and j != k):
                    res.append([chr(i+65),chr(j+65),chr(k+65)])

    return res

def combi_gears() -> [[str]] : 
    gears = [GEAR2,GEAR3,GEAR4,GEAR5,GEAR6]
    res = []
    for i in range(len(gears)):
        for j in range(len(gears)):
            for k in range(len(gears)):
                if( i != j and i != k and j != k):
                    res.append([gears[i],gears[j],gears[k]])

    return res


def decrypt_enigma(text : str , clear_gear : [str] , cipher_gear1 : [str] , cipher_gear2 : [str] , init_word : [str]) -> str :
    switch = False
    text = clean(text)
    clear_gear,cipher_gear1,cipher_gear2 = init_gears([clear_gear,cipher_gear1,cipher_gear2],init_word)
    
    
    decrypt_text = [""]*len(text)
    for i in range(len(text)):

        if(switch):
            temp = findIndex(cipher_gear1,text[i])
            shift_cg = ((-temp)%ALPHABET_LENGTH +3)%ALPHABET_LENGTH
        else:
            temp = findIndex(cipher_gear2,text[i])
            shift_cg = temp
        switch = not switch

        
        decrypt_text[i] = clear_gear[shift_cg]
        

    return decrypt_text


def break_enigma2(text : str):
    combinaison_gears = combi_gears()
    combinaison_init_words = combi_init_word()
    res = open("result.txt","w")
    for i in range(len(combinaison_gears)):
       for j in range(len(combinaison_init_words)) :
           print("CAS GEARS : ",i," WORDS : ",j)
           cg,cg1,cg2 = combinaison_gears[i]
           w = combinaison_init_words[j]

           res.write(" Words : "+str(w))
           res.write("".join(decrypt_enigma(text,cg,cg1,cg2,w)))
           res.write("\n\n")
    res.close()

# print(decrypt_enigma("JAQUH",GEAR5B,GEAR6B,GEAR7,"DOG"))

break_enigma2("VLQJQQUDXRPUUDOUWHEXRXUO")