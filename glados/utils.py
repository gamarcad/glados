###############################################################################
# File: utils.py
# Created: 09/10/19
# Updated: 11/10/19
# Authors: Gael Marcadet
# Description: Utilitaries file that's contains all help full independants 
# functions
###############################################################################

import string
from unicodedata import normalize
from typing import List, Dict


IC_ENGLISH = 0.0667
IC_FRENCH = 0.0786
IC_GERMAN = 0.0762
IC_ITALIAN = 0.0738

ALPHABET_LENGTH = 26

ALPHABET = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

FRENCH_LETTERS_WEIGHT = [
    942,102,264,339,1587,95,104,77,841,89,
    0,534,324,715,514,286,106,646,790,726,
    624,215,0,30,24,32
]

UPPERCASE_START_INDEX = 65
UPPERCASE_END_INDEX = 90
LOWERCASE_START_INDEX = 97
LOWERCASE_END_INDEX = 122

def build_key( array_of_indexs : List[int] ):
    return to_string( array_of_indexs )

def build_key_decrypt( key : str ):
    """ Returns the decrypt key """
    # decrypt key is computed as '26 - index( letter ) mod 26' for each letter
    assert type( key ) == str, "build_key_decrypt: Illegal string"

    return "".join( [ index_to_letter( ( 26 - letter_to_index( letter ) ) % 26 ) for letter in key ] )


def is_letter( index : [int, str] ) -> bool:
    """ Returns true if string is a letter """
    return is_lower( index ) or is_upper( index )

def is_upper( index : [int, str] ) -> bool:
    """ Returns true if string is an upper """
    if type( index ) == str:
        index = ord( index )
    
    return UPPERCASE_START_INDEX <= index and index <= UPPERCASE_END_INDEX

def is_lower( index : [int, str] ) -> bool:
    """ Returns true if string is a lower """
    if type( index ) == str:
        index = ord( index )
    
    return LOWERCASE_START_INDEX <= index and index <= LOWERCASE_END_INDEX

def index_to_letter( index : int ) -> str:
    """ Returns uppercase letter of specified index """
    if index <= ALPHABET_LENGTH:
        return chr( 65 + index % ALPHABET_LENGTH )
    else:
        return chr( index )
    
def letter_to_index( letter : str ) -> int:
    """ Returns the index of the letter """
    index = ord( letter )
    if is_upper( index ):
        return index - UPPERCASE_START_INDEX
    elif is_lower( index ):
        return index - LOWERCASE_START_INDEX
    else:
        raise Exception( f"letter_to_index: Undefined letter: {letter}" )

def shift( c, d ):
    """ Shift a letter with an other """
    return index_to_letter( (letter_to_index( c ) + letter_to_index( d ))%26 )


def to_string( array_of_indexs : List[int] ) -> str:
    """ Creates a string of an array of integers """
    return "".join([ index_to_letter( index ) for index in array_of_indexs ])


def text_weight( text : str ) -> float:
    """ Returns the weight of a text """
    frequences = [0] * 26
    
    for letter in text:
        frequences[ letter_to_index( letter ) ] += 1
    
    return sum( [ freq * fr for freq, fr in zip( frequences, FRENCH_LETTERS_WEIGHT ) ] ) / len( text )


NORMALIZE_NFKD = "NFKD"
def clean( text : str, upper : bool = True ) -> str:
    """ Cleans a text """
    return "".join([ 
        letter.upper() if upper else letter.lower() 
        for letter in normalize( NORMALIZE_NFKD, text ) if is_letter( letter ) 
    ])



def occurences( text : str ):
    text = clean( text )
    occrs = [ 0 ] * ALPHABET_LENGTH
    for letter in text:
        letter_index = letter_to_index( letter )
        occrs[ letter_index ] += 1
    return occrs


def repartition( text : str ):
    occs = occurences( text )
    length = sum( occs )
    return [ v / length * 100 for v in occs ]


def do_freq( text : str ) -> Dict[ str, int ]:
    freq={}
    for letter in text:
        freq[ letter ] = freq.get( letter , 0 ) + 1
    return freq

def ic( text : str ) -> float:
    """ Returns the frequency """
    freq = do_freq( text )
    
    def text_length( freq ):
        """ Returns the text length from frequencies dictionnary """
        return sum( occ for _, occ in freq.items() )
    
    # Coincidence index can computed when text has at least two caracters
    n = text_length( freq )
    if n < 2:
        return 0
    
    # Compute coincidence index
    # based on sum each letter * ( letter - 1 ) / ( text_length * ( text_length - 1 ) )
    ic = 0
    for _, occ in freq.items():
        ic += ( occ * ( occ - 1 ) ) / ( n * ( n - 1 ) )
    
    return ic

def do_ic( text : str ) -> float:
    return ic(text)

# Extrait une sous-séquence de la séquence m pour le décalage k
def extract_sub_text_list(text : str , shift : int) -> [str]:
    sub_text_list = [""]*shift

    for i in range(shift):
        sub_text_list[i] = ''.join([text[chr] for chr in range(len(text)) if chr%shift == i])

    return sub_text_list

# Calcul l'indice moyen pour un texte et un décalage donné
def compute_average_IC(text : str , key : int) -> [float]:
    sub_text_list = extract_sub_text_list(text,key)
    resultat = 0.0
    for sub_text in sub_text_list:
        resultat += ic(sub_text)
        
    resultat = resultat/len(sub_text_list)
    
    return resultat

def ic_match( text : str, language_ic : float ) -> bool:
    ''' Returns True if the text has a coincidence index greater or equals than 
    the specified language coincidence index '''
    return language_ic < ic( text )
    

