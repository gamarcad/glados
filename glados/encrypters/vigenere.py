
###############################################################################
# File: vigenere.py
# Created: 11/10/19
# Updated: 11/10/19
# Authors: Jordan Ischard
# Description: Implement the breaker and encrypter for Vigenère 
# functions
###############################################################################



from unicodedata import normalize
from glados.utils import *
import os
import sys

TEXT_LENGTH_MIN = 30

# Permet de trouver la lettre la plus présente, c'est très souvent une lettre représentant le E
def find_most_used_letter(text : str) -> str:
    freq = do_freq(text)
    l=[(freq[c],c) for c in freq]
    l.sort(reverse=True)
    
    return l[0][1]

# Donne une liste des décalages pour une liste de textes donné
def compute_shift_list(text_list : [str] , letter : str) -> [str]:
    result = []
    for text in text_list :
        most_used_letter = find_most_used_letter(text)
        result.append(shift(most_used_letter,letter))
    return result


def shift_between(text : str, most_used_letter : str) -> str :
    return (letter_to_index(find_most_used_letter(text)) - letter_to_index(most_used_letter))%26


def find_key(sub_text_list : [str] , letter : str) -> [str]:
    result_list = []
    for text in sub_text_list :
        result_list.append(index_to_letter(shift_between(text,letter)))
    return result_list
        

# Construit une clé de déchiffrement par rapport à une clé
def create_reversed_key(cle):
    return ''.join([index_to_letter(26-letter_to_index(x)) for x in cle])


# Chiffre ou déchiffre un texte codé avec la clé de Vigenère
def encrypt_Vigenere( text,cle = 'THECAKEISALIE'):

    text_clean = clean(text)
    text_encrypt = ''
    for i in range(len(text_clean)):
        text_encrypt += shift(text_clean[i],cle[i%len(cle)])

    return text_encrypt


def decrypt_vigenere( text : str, key : str ) -> str:
    """ Decrypts vigenere """
    assert type( text ) == str and type( key ) == str, "decrypt_text: Awaiting text and key as string"
    return encrypt_Vigenere( text, build_key_decrypt( key ) )


# Décode un fichier chiffré par le chiffrement de Vigenère
def break_Vigenere(filename : str , search_range : int = 50 , ic_language : float = IC_FRENCH , most_used_letter : str = 'E'):

    try :
        file = open(filename, "r")
        text_encrypt = file.read()
        text_encrypt_clean = clean(text_encrypt)

        if(len(text_encrypt_clean) > TEXT_LENGTH_MIN ):

            potential_ic_list = []
            potential_key_list =[]

            for key_length in range(1,search_range):

                potential_ic = compute_average_IC(text_encrypt_clean,key_length)
                min = (ic_language-(ic_language*50/100))
                max = (ic_language+(ic_language*50/100))
                if( min < potential_ic < max ):
                    potential_ic_list.append((key_length,potential_ic))

            
            for (key_length,ic) in potential_ic_list:

                sub_text_list = extract_sub_text_list(text_encrypt_clean,key_length)
                potential_key = find_key(sub_text_list,most_used_letter)
                potential_key_list.append((key_length,potential_key))


            for (length,cle) in potential_key_list :
                reversed_key = create_reversed_key(cle)
                res = encrypt_Vigenere( text_encrypt_clean , reversed_key )
                print("RÉSULTAT - Pour la clé ",cle,"\n le résultat est :\n",res,"\n")
                file = open("res"+str(length), "w")
                file.write(res)

        else :
            print("Texte trop petit pour utiliser ce moyen de résolution")
        file.close()

    except FileNotFoundError:
        print("Fichier non trouvé")

