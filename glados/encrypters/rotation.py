###############################################################################
# File: rotation.py
# Created: 11/10/19
# Updated: 11/10/19
# Authors: Gael Marcadet
# Description: Encrypts, decrypts and breaks rotation.
###############################################################################


from glados.utils import text_weight, clean

def rotate( text, offset ):
    import string 
    text = clean( text.upper() )
    alpha = string.ascii_uppercase
    translation = str.maketrans( alpha, alpha[ offset: ] + alpha[ :offset ] )
    return text.upper().translate( translation )


def rotation_encrypt( text, key ):
    return rotate( text, key )

def rotation_decrypt( text, key ):
    return rotation_encrypt( text, -key )

def guess_key( text ):   
    scores = [0] * 26

    for key in range( 26 ):
        plain_text = rotate( text, -key )
        scores[ key ] = text_weight( plain_text )
    
    return scores.index( max( scores ) )

def break_rotation( text ):
    guessed_key = guess_key( text )
    plain_message = rotate( text, -guessed_key )
    return ( guessed_key, plain_message )


if __name__ == "__main__":
    m = "ILOVETOMYJOBTHEREISNOCAKECHELLISINTHEPLACE"
    k = 12
    print( f"[+] Testing rotation breaker with text {m} and key {k}" )

    c = rotation_encrypt( m, k )
    print( f"[+] Encrypted: {c}" )

    decrypted_text = rotation_decrypt( c, k )
    print( f"[+] Decrypted: {decrypted_text}" )


    guess_key, guess_text = break_rotation( c )
    print( f"[+] Guessed: Key {guess_key} with text {guess_text}" )


    
    
   
