from setuptools import setup

PACKAGE_NAME  = 'glados'
VERSION = '0.1'
DESCRIPTION = 'Hack Developpers-friendly tool'
AUTHOR = 'Unnamed Team'
PACKAGES = [
    'glados'
]

DEPENDANCIES = [
    # Dependancies to create and manipulate images
    "matplotlib",
    "numpy"

]

setup(
    name = PACKAGE_NAME,
    version = VERSION,
    description = DESCRIPTION,
    author = AUTHOR,
    packages = PACKAGES,
    install_requires = DEPENDANCIES,
)